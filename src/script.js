const firstCurrency = document.getElementById("firstCurrency");
const secondCurrency = document.getElementById("secondCurrency");
const firstAmount = document.getElementById("firstAmount");
const secondAmount = document.getElementById("secondAmount");
const rateText = document.getElementById("rateText");
const swapBtn = document.querySelector(".swapBtn");

calculate();

function calculate() {
    fetch(`https://api.exchangerate-api.com/v4/latest/${firstCurrency.value}`)
    .then(response => response.json())
    .then(data => {
        const rate = data.rates[secondCurrency.value];
        const count = firstAmount.value;
        secondAmount.value = `${(count * rate)}`;
        updateRateText(rate, secondCurrency)
    });
}

swapBtn.addEventListener("click", () => {
    const current = firstCurrency.value;
    firstCurrency.value = secondCurrency.value;
    secondCurrency.value = current;
    calculate();
});


firstCurrency.addEventListener("change", calculate);
secondCurrency.addEventListener("change", calculate);
firstAmount.addEventListener("change", calculate);

function updateRateText (rate, secondCurrency) {
    rateText.innerText = `1 ${firstCurrency.value} = ${rate} ${secondCurrency.value}`;
}